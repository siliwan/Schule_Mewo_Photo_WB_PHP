<?php
//Copyright 2017, Lukas Huber
//Index file of this Project
//Setting Namespace
namespace mewo_photo_index;

$url_path =  $_SERVER['REQUEST_URI'];

//All includes here

//Main logic

//Starting Html view
?>
<html>
	<head>
		<title>My Project</title>
		<meta charset="UTF-8">
		<meta name="author" content="Lukas Huber">
		<meta name="description" content="Die offizielle Fotoausgabe der Schule Mewo">
		<!--  Please remove this four tags down below -->
		<meta http-equiv="cache-control" content="max-age=0" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<meta http-equiv="expires" content="Tue, 01 Jan 1990 12:00:00 GMT" />
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<?php echo '<link rel="stylesheet" href="' . $url_path . '/css/font-awesome.min.css">'?>
		<?php echo '<link rel="stylesheet" href="' . $url_path . '/css/mewo_default.css">'?>
	
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	
	</head>
	<body>
		  <!-- Header -->
    <header class="header" id="top">
      <div class="text-vertical-center">
        <h1>Weihnachtsball Bilder</h1>
        <h3>Schule Mewo</h3>
        <br>
        <a href="#login" class="btn btn-dark btn-lg js-scroll-trigger">Login</a>
      </div>
    </header>

    <!-- About -->
    <section id="login-container" class="login">
      <div class="container text-center">
        <h2>Hier werden sie eingeloggt</h2>
        <p id="login"/>
        </div>
      <!-- /.container -->
    </section>
	<script type="text/javascriptfunction">
    $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

    </script>
	</body>
</html>